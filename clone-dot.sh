#!/bin/sh
DOTFILES=".dotfiles"
for HOMEDIR in /mnt/home/*/
do
	HOMEDIR=${HOMEDIR%*/}
	echo 'updating: '"$HOMEDIR"
	USER="${HOMEDIR##*/}"
	git clone --bare https://gitlab.com/RudiMueller/dotfiles.git "$HOMEDIR"/"$DOTFILES"
	git --git-dir="$HOMEDIR"/"$DOTFILES"/ --work-tree="$HOMEDIR" checkout
	arch-chroot /mnt chown -R "$USER":"$USER" /home/"$USER"
done

