# Arch-installer

Dieses Script installiert mein Standard Basis Setup.
Dazu gehört eine verschlüsselte Root Partition mit LVM auf LUKS sowie ein Benutzer der mit sudo Wartungsarbeiten durchführen kann.

## Dateien

### [Scripti](./standard-installation.sh)
Parametrisierbares Script zur Installation. Fúr jeden Parameter gibt es einen Standardwert der verwendet wird wenn keine Werte übergeben werden.
Es sollten unbedingt andere Passwörter verwendet werden. Alternativ können die Passwörter nach dem ersten Starten geändert werden..
Optionen:
-H <HOSTNAME>      Hostname der Maschine ohne Domain"
                    Standardwert: rudi-tux-bot"
-u <USERNAME>      Name des initialen Nutzer. Dieser kann mit sudo seine Rechte erweitern."
                    Standardwert: rudolf"
-p <USERPASSWD>    Passwort des Benutzers."
                    Standardwert: Fridolin"
-P <ROOTPASSWD>    Passwort für Root"
                    Standardwert: EpochialerFehlschlag"
-t <TARGET_DEVICE> Laufwerk auf dem installiert werden soll (z.B. /dev/sda)"
                    Standardwert: Größtes Laufwerk
-k <KEY>           Schlüssel für die Systempartition."
                    Standardwert: Zufällige Zeichenkette die in /root/keyfile.txt gespeichert wird."
-h                 Diese Hilfe"

Das Script installiert neben dem `base` Paket auch `base-devel`, `neovim`, `zsh` und `git`.
Ausserdem wird der AUR Helper `aurman` in das $HOME Verzeichnis des angegebenen Benuzers geclont. Ich benutze dieses Tool oft und gern. Wer möchte kann
den Helper mit den üblichen Befehlen installieren.
Falls im $HOME/.ssh des Installationsmediums öffentliche Schlüssel liegen, werden diese in die Installation kopiert und dem erzeugten Benutzer als authorizierte Schlüssel konfiguriert.
Dies kann die Vorbereitung von "Dronen" vereinfachen, wenn man überall einfach einen Ansible Benutzer mit passendem Schlüssel installieren möchte. (Ansible sei hier nur als Beispiel für jegliche Wartungsökosysteme genannt.)

### [Sudoers Ordner](./suders)
Alle Dateien in diesem Ordner werden nach `/etc/sudoers.d/` kopiert. Damit wird sudo konfiguriert.
Ich aktiviere die Insults und generiere eine Benutzergruppe die mit sudo volle root Rechte erlangen kann.

### [Network Ordner](./network)
Alle Dateien in diesem Ordner werden nach `/etc/systemd/network/` kopiert. Damit wirden Netzwerkschnittstellen für Systemd-Networkd konfiguriert.
Jede Datai kein eine oder mehrere Schnittstellen konfigurieren. Dies erfolgt über ein Matching von Namen oder Eigenschaften der Schnittstellen.
Das Repository stellt eine standard Konfiguration bereit, die auf allen Schnittstellen des Namens "en\*" DHCP aktiviert.

### [Dotfiles SCript](./clone-dot.sh)
Dieses Script clont mein DotFiles Repository für alle Benutzer die ein $HOME Verzeichnis haben. Das SCript wird nicht automatisch angestossen
und kann ausgeführt werden wenn bedarf an meinen Konfigurationsdaten besteht. 

##Referenzen

Das Scipt basiert auf Informationen aus folgenden Quellen:
- [OdinsPlasmaRifle](https://gist.github.com/OdinsPlasmaRifle/e16700b83624ff44316f87d9cdbb5c94)
- [Disconnected.Systems](https://disconnected.systems/blog/archlinux-installer/#setting-variables-and-collecting-user-input)
- [Arch Linux Installation Guide](https://wiki.archlinux.org/index.php/Installation_guide)
- [Arch Linux Wiki](https>//wiki.archlinux.org)

