#!/bin/sh
script_path=$(readlink -f ${0%/*})
DIR=$(dirname "$0")
# consider to implement these as parameters:
# - localization
HOSTNAME="rudi-tux-bot"
USERNAME="rudolf"
USERPASSWD="Fridolin"
ROOTPASSWD="EpochialerFehlschlag"
TARGET_DEVICE=""
SSH_KEY="/root/.ssh/authorized_keys"
KEY=$(dd if=/dev/urandom bs=1 count=32 2>/dev/null | base64 -w 0 | rev | cut -b 2- | rev)
KEY_DEFAULT=true
DO_SHRED=false

_usage ()
{
    echo "usage ${0} [options]"
    echo
    echo " General options:"
    echo "    -H <HOSTNAME>      hostname of the target machine without domain."
    echo "                        Default: ${HOSTNAME}"
    echo "    -u <USERNAME>      name of the first user on the system. This user will be sudoer."
    echo "                        Default: ${USERNAME}"
    echo "    -p <USERPASSWD>    password for the initial user."
    echo "                        Default: ${USERPASSWD}"
    echo "    -P <ROOTPASSWD>    password for root."
    echo "                        Default: ${ROOTPASSWD}"
    echo "    -K <SSH_KEY>       public SSH key, which is copied to <USERNAME>/.ssh/ to enable public key authorization."
    echo "                        Default: ${SSH_KEY}"
    echo "    -t <TARGET_DEVICE> device which is used as system drive."
    echo "                        Default: biggest drive"
    echo "    -k <KEY>           encryption key for system disk."
    echo "                        Default: random string stored in /root/keyfile.txt"
    echo "    -s                 Shred the target device before installation."
    echo "    -h                 This help message"
    exit "${1}"
}

if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    _usage 1
fi

while getopts 'H:u:t:k:h:p:P:K:s' arg; do
    case "${arg}" in
        H) HOSTNAME="${OPTARG}" ;;
        u) USERNAME="${OPTARG}" ;;
	p) USERPASSWD="${OPTARG}" ;;
	P) ROOTPASSWD="${OPTARG}" ;;
	K) SSH_KEY="${OPTARG}" ;;
        t) TARGET_DEVICE="${OPTARG}" ;;
        k) 
	    KEY="${OPTARG}" 
	    KEY_DEFAULT=false
	    ;;
        h) _usage 0 ;;
	s) DO_SHRED=true ;;
        *)
           echo "Invalid argument '${arg}'"
           _usage 1
           ;;
    esac
done

echo 'This tool will install a default setup'
echo '--------------------------------------'

EFI=$(ls /sys/firmware/efi/)

if wget -q --spider http://archlinux.org; then
	ONLINE=true
else
	echo 'node is offline! Installation not possible!'
	exit
fi

if [ -z "$TARGET_DEVICE" ] ; then
    LOCAL_DEVICES=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
    TARGET_DEVICE=$(echo "$LOCAL_DEVICES" | sed -n '1p' | awk '{ print $1 }')
fi
echo 'Device used for installation: '"$TARGET_DEVICE"

timedatectl set-ntp true
sed -i 's/^#\(.*hs-esslingen.*\)$/\1/' /etc/pacman.d/mirrorlist

if [ -z "$EFI" ] ; then
	echo 'EFI systems only!'
	exit 0
fi
echo 'partitioning in process'
if "$DO_SHRED" ; then
    shred -v -n1 "$TARGET_DEVICE"
fi

sgdisk -Z "$TARGET_DEVICE"
sgdisk -n 1:0:+512M -c 1:"EFI partition" -u 1:'R' -t 1:ef00 "$TARGET_DEVICE"
sgdisk -n 2:0:0 -c 2:"Root" -u 2:'R' -t 2:8e00 "$TARGET_DEVICE"

PART_BOOT="$TARGET_DEVICE"1
PART_ROOT="$TARGET_DEVICE"2

wipefs "$PART_BOOT"
wipefs "$PART_ROOT"

mkfs.fat -F 32 "$PART_BOOT"

echo 'Encryption...'
modprobe dm-crypt
echo "$KEY" > /root/keyfile.txt

echo "$KEY" | cryptsetup -q luksFormat --type luks2 "$PART_ROOT"
echo "$KEY" | cryptsetup open --type luks "$PART_ROOT" lvm
pvcreate /dev/mapper/lvm
vgcreate volume /dev/mapper/lvm
lvcreate -l 100%FREE volume -n root
mkfs.ext4 /dev/mapper/volume-root

echo 'mount partitions...'
mount /dev/mapper/volume-root /mnt
mkdir /mnt/boot
mount "$PART_BOOT" /mnt/boot

echo 'Install packages...'
pacstrap /mnt base linux linux-firmware sudo openssh dash efibootmgr intel-ucode lvm2
pacstrap /mnt zsh zsh-doc zsh-completions zsh-syntax-highlighting
pacstrap /mnt man-pages man-db texinfo 
pacstrap /mnt neovim git base-devel cowsay fzy

echo 'Post install confirugaration...'
genfstab -U /mnt >> /mnt/etc/fstab

echo "$HOSTNAME" > /mnt/etc/hostname

sed -i 's/^\(HOOKS.*\s\)\(filesystems.*\s\)keyboard\s\(.*\)$/\1keyboard encrypt lvm2 \2\3/' /mnt/etc/mkinitcpio.conf
arch-chroot /mnt mkinitcpio -p linux
arch-chroot /mnt bootctl install

ROOT_UUID=$(blkid -s UUID -o value "$PART_ROOT")
echo 'default arch' > /mnt/boot/loader/loader.conf
echo 'timeout  4' >> /mnt/boot/loader/loader.conf
echo 'console-mode max' >> /mnt/boot/loader/loader.conf
echo 'editor   no' >> /mnt/boot/loader/loader.conf

echo 'title    Arch Linux' > /mnt/boot/loader/entries/arch.conf
echo 'linux    /vmlinuz-linux' >> /mnt/boot/loader/entries/arch.conf
echo 'initrd   /intel-ucode.img' >> /mnt/boot/loader/entries/arch.conf
echo 'initrd   /initramfs-linux.img' >> /mnt/boot/loader/entries/arch.conf
echo "options  cryptdevice=UUID=""$ROOT_UUID"":volume root=/dev/mapper/volume-root rw" >> /mnt/boot/loader/entries/arch.conf

echo 'title    Arch Linux - Fallback' > /mnt/boot/loader/entries/arch-fallback.conf
echo 'linux    /vmlinuz-linux' >> /mnt/boot/loader/entries/arch-fallback.conf
echo 'initrd   /intel-ucode.img' >> /mnt/boot/loader/entries/arch-fallback.conf
echo 'initrd   /initramfs-linux-fallback.img' >> /mnt/boot/loader/entries/arch-fallback.conf
echo "options  cryptdevice=UUID=""$ROOT_UUID"":volume root=/dev/mapper/volume-root rw" >> /mnt/boot/loader/entries/arch-fallback.conf

arch-chroot /mnt ln -sf /usr/share/zoneinfo/Europe/Berlin /etc/localtime
arch-chroot /mnt hwclock --systohc
arch-chroot /mnt timedatectl set-ntp true

sed -i 's/#\(de_DE\.UTF-8\)/\1/' /mnt/etc/locale.gen
sed -i 's/#\(de_DE ISO-8859-1\)/\1/' /mnt/etc/locale.gen
sed -i 's/#\(de_DE@euro ISO-8859-15\)/\1/' /mnt/etc/locale.gen
arch-chroot /mnt locale-gen

echo "LANG=de_DE.UTF-8" > /mnt/etc/locale.conf
echo "KEYMAP=de" >> /mnt/etc/locale.conf


echo "127.0.0.1 localhost" > /mnt/etc/hosts
echo "::1 localhost" >> /mnt/etc/hosts
echo "127.0.1.1 ""$HOSTNAME"".rudis-infrastruktur.de ""$HOSTNAME" >> /mnt/etc/hosts

arch-chroot /mnt systemctl enable systemd-networkd
arch-chroot /mnt systemctl enable systemd-resolved

sed -i 's/^#\(.*hs-esslingen.*\)$/\1/' /mnt/etc/pacman.d/mirrorlist

arch-chroot /mnt groupadd users
arch-chroot /mnt useradd -m -G wheel,users -s /usr/bin/zsh "$USERNAME"
arch-chroot /mnt chsh -s /usr/bin/zsh

echo "$USERNAME:$USERPASSWD" | chpasswd --root /mnt
echo "root:$ROOTPASSWD" | chpasswd --root /mnt

cp -R "$DIR"/etc/* /mnt/etc/

mkdir -p /mnt/home/"$USERNAME"/.ssh
touch /mnt/home/"$USERNAME"/.ssh/authorized_keys
arch-chroot /mnt chown -R "$USERNAME":"$USERNAME" /home/"$USERNAME"/.ssh/
cat "$SSH_KEY" >> /mnt/home/"$USERNAME"/.ssh/authorized_keys
chmod 700 /mnt/home/"$USERNAME"/.ssh
chmod 600 /mnt/home/"$USERNAME"/.ssh/authorized_keys

mkdir -p /mnt/home/"$USERNAME"/packages
git -C /mnt/home/"$USERNAME"/packages clone https://aur.archlinux.org/aurman.git
chown -R "$USERNAME":"$USERNAME" /mnt/home/"$USERNAME"/packages

echo '-- installation finished'

