# Anleitung für das Starten mit Schlüsseldatei auf externem Laufwerk

## Vorbereitung
### Partitionierung
Die Schlüsseldatei ist wenige Kilo-Byte groß. Entsprechend kann die benutzte Partition dimensioniert sein.
`fdisk /dev/sdX`

### Formatierung
Formatiere die Partition mit einem Standard Dateisystem.
`mkfs.ext4 /dev/sdXY`

### Einbinden
`mount /dev/sdXY /mnt`

## Schlüsseldatei erstellen
Ich bevorzuge zufällige Daten in den Schlüsseldateien. Grundsätzlich kann jede Datei verwendet werden.
`dd if=/dev/urandom of=/mnt/key bs=1024 count=8`

Die Datei darf nur von root lesbar sein.
`chmod 0400 /mnt/key`

## LUKS konfigurieren
Die Schlüsseldatei kann nun an das LUKS Gerät gebunden werden.
Cryptsetup wird nach einer gültigen Passphrase fragen, um die Aktion auszuführen.
`cryptsetup luksAddKey /dev/<root-partition> /mnt/key`
Je nach Schlüsseldateigröße dauert das Hinzufügen unterschiedlich lang.

## Bootloader konfigurieren
Im Eintrag des Bootloaders muss die Schlüsseldatei definiert werden. 
`options cryptdevice=... cryptkey=UUID=<UUID der Partition mit dem Schlüssel>:ext4:key root=...`
Die UUID kann wie folgt bestimmt werden:
`blkid -s UUID -o value /dev/sdXY`

# Weiterführende Infos
Wie immer ist empfohlen die Arch-Wiki und die Dokumentation von LUKS zu konsultieren.
[Arch-Wki](https://wiki.archlinux.org/index.php/Dm-crypt/Encrypting_an_entire_system#Preparing_the_boot_partition)
